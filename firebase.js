import * as firebase from 'firebase'
let database

export const init = () => {
  let config = {
    apiKey: "AIzaSyDstcbFc49LeFEa0nZc3sjyicJDmik1-fY",
    authDomain: "bloggie-cd4d2.firebaseapp.com",
    databaseURL: "https://bloggie-cd4d2.firebaseio.com",
    storageBucket: "bloggie-cd4d2.appspot.com",
    messagingSenderId: "495148342662"
  }

  if (!firebase.apps.length) {
    firebase.initializeApp(config)
  }
  database = firebase.database()
}


// retrieve posts from firebase
// return promise object
export const getPostsDB = () => {
  return database.ref('/').once('value')
}

// get specified post
export const getPostDB = (postId) => {
  return database.ref(`/${postId}`).once('value')
}

// Delete specified post
export const deletePostDB = (postId) => {
  return database.ref(`/${postId}`).once('value').then((snapshot) => {
    snapshot.ref.remove()
    return { deleted: true }
  })
  .catch((error) => ({ error }))
}

// add new post
export const addPostDB = (title, description) => {
  let key = database.ref('/').push().key
  return database.ref('/'+ key).set({
    id: key,
    title,
    description,
    timestamp: firebase.database.ServerValue.TIMESTAMP
  }).then(() => {
    return { postId: key }
  }).catch((error) => { error })
}

// edit post
export const editPostDB = (postId, title, description) => {
  return database.ref(`/${postId}`).update({
    title,
    description,
  }).then(() => {
    return { success: true }
  }).catch((error) => { error })
}