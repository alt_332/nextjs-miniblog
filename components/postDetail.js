import Link from 'next/link'
import { connect } from 'react-redux'
import {
  ShareButtons,
  ShareCounts,
  generateShareIcon
} from 'react-share';

const {
  FacebookShareButton,
  TwitterShareButton,
  GooglePlusShareButton,
  LinkedinShareButton
} = ShareButtons;

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');
const LinkedinIcon = generateShareIcon('linkedin');

import Layout from './BlogLayout'

function PostDetail ({loadPostDetailError, postDetail, postDetailLoading}) {
  return (
    <Layout>
      <Link as="/" href="/">
        <a>Back</a>
      </Link>
      <br />
      <br />
      { postDetailLoading ?
        <div className="loading">Loading...</div>
        :
        postDetail !== null ?
        <div className="row">
          <div className="post-detail column column-80 column-offset-10">
            <h4><b>{postDetail.title}</b></h4>
            <br />
            {postDetail.description}
          </div>
          <div className="column column-80">
          <FacebookShareButton
            url={window.location.href}
            quote={postDetail.title}
          >
            <FacebookIcon
              size={32}
              round />
          </FacebookShareButton>
          <TwitterShareButton
            url={window.location.href}
            title={postDetail.title}
          >
            <TwitterIcon
              size={32}
              round />
          </TwitterShareButton>
          <GooglePlusShareButton
            url={window.location.href}
          >
            <GooglePlusIcon
              size={32}
              round />
          </GooglePlusShareButton>
          <LinkedinShareButton
            url={window.location.href}
            title={postDetail.title}
            windowWidth={750}
            windowHeight={600}
          >
            <LinkedinIcon
              size={32}
              round />
          </LinkedinShareButton>
          </div>
        </div>
        :
        <div />
      }
      {
        loadPostDetailError ?
        <center><p>{loadPostDetailError}</p></center>
        :
        <div />
      }
    </Layout>
  )
}

export default connect(state => state)(PostDetail)