import React, { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'
import { connect } from 'react-redux'

import Layout from './BlogLayout'

class EditPostForm extends Component {
  constructor() {
    super()

    this.state = {
      title: '',
      description: ''
    }
  }

  handleInputChange(e, name) {
    const target = e.target;
    const value = target.value;

    this.setState({
      [name]: value
    });
  }

  componentWillReceiveProps(props) {
    if (props.postDetail) {
      this.setState({
        title: props.postDetail.title,
        description: props.postDetail.description,
      })
    }

    if (props.edited) {
      Router.push({
        pathname: '/post',
        query: { id: props.postDetail.id }
      })
    }
  }

  render() {
    return (
      <Layout>
        <Link as="/" href="/">
          <a>Back</a>
        </Link>
        <br />
        <br />
        { 
          this.props.editing ?
          <div>Editing...</div>
          :
          this.props.postDetailLoading ?
          <div className="loading">Loading...</div>
          :
          <form>
            <fieldset>
              <label htmlFor="Title">Title</label>
              <input
                value={this.state.title}
                onChange={(e, name) => this.handleInputChange(e, 'title')}
                type="text"
                placeholder="Title"
              />
              <label htmlFor="commentField">Description</label>
              <textarea
                value={this.state.description}
                placeholder="Description"
                onChange={(e, name) => this.handleInputChange(e, 'description')}
              />
              <input
                className="button-primary"
                type="submit"
                value="Publish"
                onClick={(title, description, e) => this.props.handleSubmit(this.state.title, this.state.description, e)}
              />
            </fieldset>
          </form>
        }
      </Layout>
    )
  }
}

export default connect(state => state)(EditPostForm)