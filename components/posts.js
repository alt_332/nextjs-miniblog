import React, { Component } from 'react'
import Link from 'next/link'
import { connect } from 'react-redux'
import { loadPosts } from '../actions'

import Layout from './BlogLayout'

const postStyle = {
  backgroundColor: 'white',
  boxShadow: '0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12)'
}

class Posts extends Component {
  componentWillReceiveProps(props) {
    if (props.deleted) {
      this.props.dispatch(loadPosts())
    }
  }

  render() {
    const { posts, postsLoading } = this.props

    return (
      <Layout>
        { postsLoading ?
          <div className="loading">Loading...</div>
          :
          posts !== null ? Object.keys(posts).map((key) => (
            <div key={key} className="post row">
              <div style={postStyle} className="column column-50 column-offset-25">
                <h4><b>{posts[key].title}</b></h4>
                {posts[key].description} ...
                <Link as={`/post/${posts[key].id}`} href={`/post?id=${posts[key].id}`}>
                  <a>More</a>
                </Link>
                <hr />
                <div className="clear-fix">
                  <div className="float-left">
                    <Link as={`/edit/${posts[key].id}`} href={`/edit?id=${posts[key].id}`}>
                      <button className="button button-outline">Edit</button>
                    </Link>
                  </div>
                  <div className="float-right">
                    <button className="button" onClick={(postId) => this.props.handleDelete(posts[key].id)}>Delete</button>
                  </div>
                </div>
              </div>
            </div>
          ))
          :
          <div />
        }
      </Layout>
    )
  }
  
}

export default connect(state => state)(Posts)