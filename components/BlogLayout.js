import React from 'react'
import Link from 'next/link'

const Layout = (props) => {
  return (
    <div className="container">
      <div className="clearfix">

        <div className="float-left">
          <h1>Mini Blog</h1>
        </div>
        <div className="float-right">
          <Link href="/new">
            <button className="button button-clear">New</button>
          </Link>
        </div>

      </div>
      <main>
        {props.children}
      </main>
    </div>
  )
}

export default Layout