import React, { Component } from 'react'
import Link from 'next/link'
import Router from 'next/router'
import { connect } from 'react-redux'

import Layout from './BlogLayout'

class NewPostForm extends Component {
  componentWillReceiveProps(props) {
    if (props.added) {
      Router.push({
        pathname: '/post',
        query: { id: props.addedPostId }
      })
    }
  }

  render() {
    return (
      <Layout>
        <Link as="/" href="/">
          <a>Back</a>
        </Link>
        <br />
        <br />
        <form>
          <fieldset>
            <label htmlFor="Title">Title</label>
            <input
              onChange={(e, name) => this.props.handleInputChange(e, 'title')}
              type="text"
              placeholder="Title"
            />
            <label htmlFor="commentField">Description</label>
            <textarea
              placeholder="Description"
              onChange={(e, name) => this.props.handleInputChange(e, 'description')}
            />
            <input
              className="button-primary"
              type="submit"
              value="Publish"
              onClick={this.props.handleSubmit}
             />
          </fieldset>
        </form>
      </Layout>
    )
  }
}

export default connect(state => state)(NewPostForm)