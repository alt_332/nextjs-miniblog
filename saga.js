/* global fetch */

import { all, call, put, takeLatest } from 'redux-saga/effects'

import {
  getPostsDB,
  addPostDB,
  getPostDB,
  editPostDB,
  deletePostDB
} from './firebase'

import {
  actionTypes,
  loadPostsSuccess,
  loadPostDetailSuccess,
  loadPosts,
  loadPostsFail,
  loadPostDetailFail,
  deletePostSuccess,
  deletePostFail,
  addPostSuccess,
  addPostFail,
  editPostSuccess,
  editPostFail
} from './actions'

function * loadPostDetailSaga ({ postId }) {
  try {
    const res = yield getPostDB(postId)
    const data = yield res.val()
    if (data) {
      yield put(loadPostDetailSuccess(data))
    } else {
      yield put(loadPostDetailFail('Not Found!'))
    }
  } catch (err) {
    yield put(loadPostDetailFail(err))
  }
}

function * deletePostSaga ({ postId }) {
  const { deleted, error } = yield call(deletePostDB, postId)
  if (deleted) {
    yield put(deletePostSuccess())
  } else if (error) {
    yield put(deletePostFail(error))
  }
}

function * addPostSaga ({ title, description }) {
  const { postId, error } = yield call(addPostDB, title, description)
  if (postId) {
    yield put(addPostSuccess(postId))
  } else if (error) {
    yield put(addPostFail(error))
  }
}

function * editPostSaga ({ postId, title, description }) {
  const { success, error } = yield call(editPostDB, postId, title, description)
  if (success) {
    yield put(editPostSuccess())
  } else if (error) {
    yield put(editPostFail(error))
  }
}

function * loadPostsSaga () {
  try {
    const res = yield getPostsDB()
    const data = yield res.val()
    yield put(loadPostsSuccess(data))
  } catch (err) {
    yield put(loadPostsFail(err))
  }
}

function * rootSaga () {
  yield all([
    takeLatest(actionTypes.LOAD_POSTS, loadPostsSaga),
    takeLatest(actionTypes.LOAD_POST_DETAIL, loadPostDetailSaga),
    takeLatest(actionTypes.DELETE_POST, deletePostSaga),
    takeLatest(actionTypes.ADD_POST, addPostSaga),
    takeLatest(actionTypes.EDIT_POST, editPostSaga),
  ])
}

export default rootSaga