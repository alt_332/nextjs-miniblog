import { actionTypes } from './actions'

export const initialState = {
  loadPostsError: false,
  loadPostDetailError: false,
  posts: null,
  postDetail: null,
  postsLoading: false,
  postDetailLoading: false,
  deletePostError: null,
  editPostError: null,
  deleting: false,
  deleted: false,
  editing: false,
  edited: false,
  adding: false,
  added: false,
  addedPostId: null,
  addPostError: null
}

function reducer (state = initialState, action) {
  switch (action.type) {
    case actionTypes.ADD_POST:
      return {
        ...state,
        adding: true,
        added: false,
        addPostError: null
      }

    case actionTypes.ADD_POST_SUCCESS:
      return {
        ...state,
        addedPostId: action.postId,
        adding: false,
        added: true,
        addPostError: null
      }
    
    case actionTypes.ADD_POST_FAIL:
      return {
        ...state,
        adding: false,
        added: false,
        addPostError: action.error
      }
    
    case actionTypes.EDIT_POST:
      return {
        ...state,
        editing: true,
        edited: false,
        editPostError: null
      }

    case actionTypes.EDIT_POST_SUCCESS:
      return {
        ...state,
        editing: false,
        edited: true,
        editPostError: null
      }
    
    case actionTypes.EDIT_POST_FAIL:
      return {
        ...state,
        editing: false,
        edited: false,
        editPostError: action.error
      }

    case actionTypes.LOAD_POSTS_FAIL:
      return {
        ...state,
        loadPostsError: action.error,
        postsLoading: false,
        posts: null
      }

    case actionTypes.LOAD_POST_DETAIL_FAIL:
      return {
        ...state,
        loadPostDetailError: action.error,
        postDetailLoading: false,
        postDetail: null
      }
    
    case actionTypes.LOAD_POST_DETAIL:
      return {
        ...state,
        postDetailLoading: true,
        loadPostDetailError: false,
        postDetail: null
      }

    case actionTypes.LOAD_POSTS:
      return {
        ...state,
        addedPostId: null,
        edited: false,
        added: false,
        deleted: false,
        loadPostsError: false,
        posts: null,
        postsLoading: true
      }

    case actionTypes.DELETE_POST:
      return {
        ...state,
        deletePostError: false,
        deleting: true
      }

    case actionTypes.DELETE_POST_SUCCESS:
      return {
        ...state,
        deletePostError: false,
        deleting: false,
        deleted: true
      }
    
    case actionTypes.DELETE_POST_FAIL:
      return {
        ...state,
        deletePostError: action.error,
        deleting: false
      }

    case actionTypes.LOAD_POSTS_SUCCESS:
      return {
        ...state,
        posts: action.data,
        postsLoading: false,
        loadPostsError: false
      }
    
     case actionTypes.LOAD_POST_DETAIL_SUCCESS:
      return {
        ...state,
        postDetail: action.data,
        postDetailLoading: false,
        loadPostDetailError: false
      }
      

    default:
      return state
  }
}

export default reducer