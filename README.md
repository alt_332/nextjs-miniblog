# NextJS-Mini-Blog

## How to use

Install and run:

```bash
npm install || yarn install
npm run start || yarn start
```

Deploy it to the cloud with [now](https://zeit.co/now) ([download](https://zeit.co/download))

```bash
now
```