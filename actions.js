export const actionTypes = {
  LOAD_POSTS_FAIL: 'LOAD_POSTS_FAIL',
  LOAD_POST_DETAIL_FAIL: 'LOAD_POST_DETAIL_FAIL',
  LOAD_POSTS: 'LOAD_POSTS',
  DELETE_POST: 'DELETE_POST',
  LOAD_POST_DETAIL: 'LOAD_POST_DETAIL',
  LOAD_POSTS_SUCCESS: 'LOAD_POSTS_SUCCESS',
  LOAD_POST_DETAIL_SUCCESS: 'LOAD_POST_DETAIL_SUCCESS',
  DELETE_POST_SUCCESS: 'DELETE_POST_SUCCESS',
  DELETE_POST_FAIL: 'DELETE_POST_FAIL',
  ADD_POST: 'ADD_POST',
  ADD_POST_SUCCESS: 'ADD_POST_SUCCESS',
  ADD_POST_FAIL: 'ADD_POST_FAIL',
  EDIT_POST: 'EDIT_POST',
  EDIT_POST_SUCCESS: 'EDIT_POST_SUCCESS',
  EDIT_POST_FAIL: 'EDIT_POST_FAIL'
}

export function addPost(title, description) {
  return {
    type: actionTypes.ADD_POST,
    title,
    description
  }
}

export function addPostSuccess(postId) {
  return {
    type: actionTypes.ADD_POST_SUCCESS,
    postId
  }
}

export function addPostFail(error) {
  return {
    type: actionTypes.ADD_POST_FAIL,
    error
  }
}

export function editPost(postId, title, description) {
  return {
    type: actionTypes.EDIT_POST,
    postId,
    title,
    description
  }
}

export function editPostSuccess() {
  return {
    type: actionTypes.EDIT_POST_SUCCESS
  }
}

export function editPostFail(error) {
  return {
    type: actionTypes.EDIT_POST_FAIL,
    error
  }
}

export function loadPostsFail (error) {
  return {
    type: actionTypes.LOAD_POSTS_FAIL,
    error
  }
}

export function loadPostDetailFail (error) {
  return {
    type: actionTypes.LOAD_POST_DETAIL_FAIL,
    error
  }
}

export function loadPosts () {
  return {
    type: actionTypes.LOAD_POSTS
  }
}

export function loadPostDetail (postId) {
  return {
    type: actionTypes.LOAD_POST_DETAIL,
    postId
  }
}

export function deletePost (postId) {
  return {
    type: actionTypes.DELETE_POST,
    postId
  }
}

export function deletePostSuccess () {
  return {
    type: actionTypes.DELETE_POST_SUCCESS
  }
}

export function deletePostFail (error) {
  return {
    type: actionTypes.DELETE_POST_FAIL,
    error
  }
}

export function loadPostsSuccess (data) {
  return {
    type: actionTypes.LOAD_POSTS_SUCCESS,
    data
  }
}

export function loadPostDetailSuccess (data) {
  return {
    type: actionTypes.LOAD_POST_DETAIL_SUCCESS,
    data
  }
}