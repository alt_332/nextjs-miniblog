import React, { Component } from 'react'

import { loadPosts, deletePost } from '../actions'
import { withReduxSaga } from '../store'
import { init as firebaseInit, addPost } from '../firebase'
import Posts from '../components/posts'

firebaseInit()

class Index extends Component {
  componentDidMount() {
    // addPost('hello', 'world')
    this.props.dispatch(loadPosts())
  }

  refreshPage() {
    this.props.dispatch(loadPosts())
  }

  handleDeletePost(postId) {
    let result = confirm("Are you sure?")

    if (result) {
      this.props.dispatch(deletePost(postId))
    }
  }

  render () {
    return <Posts
      handleDelete={(postId) => this.handleDeletePost(postId)}
      refreshPage={() => this.refreshPage()}
    />
  }
}

export default withReduxSaga(Index)