import React, { Component } from 'react'

import { addPost, loadPostDetail } from '../actions'
import { withReduxSaga } from '../store'
import { init as firebaseInit } from '../firebase'
import Layout from '../components/BlogLayout'
import NewPostForm from '../components/newPostForm'

firebaseInit();

class NewPost extends Component {
  constructor() {
    super();

    this.state = {
      title: '',
      description: ''
    };
  }

  handleInputChange(e, name) {
    const target = e.target;
    const value = target.value;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(e) {
    if (!this.state.title || !this.state.description) {
      alert('Please fill all fields.')
    } else {
      this.props.dispatch(addPost(this.state.title, this.state.description))
    }
    e.preventDefault()
  }

  render () {
    return <NewPostForm
      handleSubmit={(e) => this.handleSubmit(e)}
      handleInputChange={(e, name) => this.handleInputChange(e, name)}
    />
  }
}

export default withReduxSaga(NewPost)