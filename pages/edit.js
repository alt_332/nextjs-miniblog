import React, { Component } from 'react'

import { loadPostDetail, editPost } from '../actions'
import { withReduxSaga } from '../store'
import { init as firebaseInit } from '../firebase'
import Layout from '../components/BlogLayout'
import EditPostForm from '../components/editPostForm'

firebaseInit();

class Edit extends Component {
  componentDidMount() {
    this.props.dispatch(loadPostDetail(this.props.url.query.id))
  }

  handleSubmit(title, description, e) {
    if(!title || !description) {
      alert('Please fill all forms!')
    } else {
      this.props.dispatch(editPost(this.props.url.query.id, title, description))
    }
  }

  render () {
    return <EditPostForm
      handleSubmit={(title, description) => this.handleSubmit(title, description)}
    />
  }
}

export default withReduxSaga(Edit)