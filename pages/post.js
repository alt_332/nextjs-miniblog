import React, { Component } from 'react'

import { loadPostDetail } from '../actions'
import { withReduxSaga } from '../store'
import { init as firebaseInit } from '../firebase'
import PostDetail from '../components/postDetail'

firebaseInit()

class Post extends Component {
  componentDidMount() {
    this.props.dispatch(loadPostDetail(this.props.url.query.id))
  }

  render () {
    return <PostDetail />
  }
}

export default withReduxSaga(Post)